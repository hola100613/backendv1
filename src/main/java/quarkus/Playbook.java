package quarkus;

import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.util.Objects;

@Entity
public class Playbook {
    
    @Id
    private Long idPlaybook;
    
    private String NombrePlaybook;

    private int AnsiblePlaybook;

    public Long getIdPlaybook() {
        return idPlaybook;
    }

    public void setIdPlaybook(Long idPlaybook) {
        this.idPlaybook = idPlaybook;
    }

    public String getNombrePlaybook() {
        return NombrePlaybook;
    }

    public void setNombrePlaybook(String NombrePlaybook) {
        this.NombrePlaybook = NombrePlaybook;
    }

    public int getAnsiblePlaybook() {
        return AnsiblePlaybook;
    }

    public void setAnsiblePlaybook(int AnsiblePlaybook) {
        this.AnsiblePlaybook = AnsiblePlaybook;
    }

    @Override
    public int hashCode() {
        return Objects.hash(NombrePlaybook, AnsiblePlaybook);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Playbook other = (Playbook) obj;
        if (this.idPlaybook != other.idPlaybook) {
            return false;
        }
        if (this.AnsiblePlaybook != other.AnsiblePlaybook) {
            return false;
        }
        return Objects.equals(this.NombrePlaybook, other.NombrePlaybook);
    }

    @Override
    public String toString() {
        return "Playbook{" + "idPlaybook=" + idPlaybook + ", NombrePlaybook=" + NombrePlaybook + ", AnsiblePlaybook=" + AnsiblePlaybook + '}';
    }



}
