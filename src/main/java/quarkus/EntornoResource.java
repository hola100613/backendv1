package quarkus;

import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/Entornos")
@Transactional
@Authenticated
public class EntornoResource {
    
    @Inject
    private EntornoRepository entornoRepository;

    @GET
    public List<Entorno> index(){
        return entornoRepository.listAll();
    }

    @POST
    public Entorno insert(Entorno entorno){
        entornoRepository.persist(entorno);
        return entorno;
    }

    @GET
    @Path("{idEntorno}")
    public Entorno retrieve(@PathParam("idEntorno") Long id){
        var entorno = entornoRepository.findById(id);
        if (entorno != null){
            return entorno;
        }
        throw new NoSuchElementException("No existe Entorno con esta id" +id+".");
    }

    @DELETE
    @Path("{idEntorno}")
    public String delete(@PathParam("idEntorno") Long id){
        if (entornoRepository.deleteById(id)){
            return "Entorno eliminado";
        }
        else{
            return "no se encontro Entorno ah eliminar";
        }
    }

    @PUT
    @Path ("{idEntorno}")
    public Entorno update(@PathParam("idEntorno") Long id, Entorno entorno){
        var updateEntorno = entornoRepository.findById(id);
        if (updateEntorno != null){
            updateEntorno.setNombreEntorno(entorno.getNombreEntorno());
            updateEntorno.setAnsibleEntorno(entorno.getAnsibleEntorno());
        }
        throw new NoSuchElementException("No existe Entorno de id"+ id+ "para editar.");
    }
}
