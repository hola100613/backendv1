package quarkus;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CredentialsRepository implements PanacheRepository<Credentials> {
    
}
