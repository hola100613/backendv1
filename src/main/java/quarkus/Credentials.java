package quarkus;

import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
public class Credentials {
    
    @Id
    private Long idCredentials;

    private String NombreCredentials;

    private int AnsibleCredentials;

    public Long getIdCredentials() {
        return idCredentials;
    }

    public void setIdCredentials(Long idCredentials) {
        this.idCredentials = idCredentials;
    }

    public String getNombreCredentials() {
        return NombreCredentials;
    }

    public void setNombreCredentials(String NombreCredentials) {
        this.NombreCredentials = NombreCredentials;
    }

    public int getAnsibleCredentials() {
        return AnsibleCredentials;
    }

    public void setAnsibleCredentials(int AnsibleCredentials) {
        this.AnsibleCredentials = AnsibleCredentials;
    }

    @Override
    public int hashCode() {
        return Objects.hash(NombreCredentials, AnsibleCredentials);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Credentials other = (Credentials) obj;
        if (this.idCredentials != other.idCredentials) {
            return false;
        }
        if (this.AnsibleCredentials != other.AnsibleCredentials) {
            return false;
        }
        return Objects.equals(this.NombreCredentials, other.NombreCredentials);
    }

    @Override
    public String toString() {
        return "Credentials{" + "idCredentials=" + idCredentials + ", NombreCredentials=" + NombreCredentials + ", AnsibleCredentials=" + AnsibleCredentials + '}';
    }
    
    
}
