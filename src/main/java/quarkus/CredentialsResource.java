package quarkus;

import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("/Credentials")
@Transactional
@Authenticated
public class CredentialsResource {
    
    @Inject
    private CredentialsRepository credentialsRepository;

    @GET
    public List<Credentials> index(){
        return credentialsRepository.listAll();
    }

    @POST
    public Credentials insert(Credentials credentials){
        credentialsRepository.persist(credentials);
        return credentials;
    }

    @GET
    @Path("{idCredentials}")
    public Credentials retrieve(@PathParam("idCredentials") Long id){
        var credentials = credentialsRepository.findById(id);
        if (credentials != null){
            return credentials;
        }
        throw new NoSuchElementException("No existe Credencial  con esta id" +id+".");
    }

    @DELETE
    @Path("{idCredentials}")
    public String delete(@PathParam("idCredentials") Long id){
        if (credentialsRepository.deleteById(id)){
            return "Credencial eliminado";
        }
        else{
            return "no se encontro Credentials ah eliminar";
        }
    }

    @PUT
    @Path ("{idCredentials}")
    public Credentials update(@PathParam("idCredentials") Long id, Credentials credentials){
        var updateCredentials = credentialsRepository.findById(id);
        if (updateCredentials != null){
            updateCredentials.setNombreCredentials(credentials.getNombreCredentials());
            updateCredentials.setAnsibleCredentials(credentials.getAnsibleCredentials());
        }
        throw new NoSuchElementException("No existe Credentials de id"+ id+ "para editar.");
    }
}
