package quarkus;

import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
public class Entorno {
    
    @Id
    private Long idEntorno;

    private String NombreEntorno;

    private int AnsibleEntorno;

    public Long getIdEntorno() {
        return idEntorno;
    }

    public void setIdEntorno(Long idEntorno) {
        this.idEntorno = idEntorno;
    }

    public String getNombreEntorno() {
        return NombreEntorno;
    }

    public void setNombreEntorno(String NombreEntorno) {
        this.NombreEntorno = NombreEntorno;
    }

    public int getAnsibleEntorno() {
        return AnsibleEntorno;
    }

    public void setAnsibleEntorno(int AnsibleEntorno) {
        this.AnsibleEntorno = AnsibleEntorno;
    }

    @Override
    public int hashCode() {
        return Objects.hash(NombreEntorno, AnsibleEntorno);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entorno other = (Entorno) obj;
        if (this.idEntorno != other.idEntorno) {
            return false;
        }
        if (this.AnsibleEntorno != other.AnsibleEntorno) {
            return false;
        }
        return Objects.equals(this.NombreEntorno, other.NombreEntorno);
    }

    @Override
    public String toString() {
        return "Entorno{" + "idEntorno=" + idEntorno + ", NombreEntorno=" + NombreEntorno + ", AnsibleEntorno=" + AnsibleEntorno + '}';
    }

}
